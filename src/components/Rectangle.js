import React from 'react'

const Rectangle = React.forwardRef((props,ref) =>(
        <div style={{width:props.width,height:props.height}} className="rectangle">
     
            {props.children}
        </div>
    ))

export default Rectangle
