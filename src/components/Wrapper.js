import React,{useState,useRef,useEffect} from 'react';
import styled from 'styled-components'
import Rectangle from './Rectangle'
import RectangleResult from './RectangleResult'
const Wrapper = () => {
    const [ widthState, setWidth ] = useState(103)
    const [ heightState, setHeight ] = useState(120)
    const [ maxState, setMax ] = useState(500)
    const [ minState, setMin ] = useState(100)
    
    
    const handleResize = () =>{
        const max = maxState
        const min = minState
        let width = Math.floor(Math.random() * (max-min) + min)
        let height = Math.floor(Math.random() * (max-min) + min)
        setWidth(width)
        setHeight(height)
    }
    const myRef =  React.createRef()
    return (
        <WrapperStyle>
        <div className="wrapper">
            <Rectangle ref={myRef} width={widthState} height={heightState}>
                <RectangleResult width={widthState} height={heightState}/>
            </Rectangle>
            <div className="width">Szerokosc {widthState} px</div>
            <div className="height">Wysokosc {heightState} px</div>
            <button className="btn" onClick={()=>{handleResize()}}>Generuj nowy Element</button>
        </div>
    </WrapperStyle>
    );
}

const WrapperStyle = styled.div`
    .rectangle{
        margin:0px auto;
        display:flex;
        justify-content:center;
        align-items:center;
        ${'' /* width:${props=>props.width+'px'}; */}
        ${'' /* height:${props=>props.height+'px'}; */}
        color:white;
        background-color:black;
        transition:all 500ms ease-in-out;
    }
`
export default Wrapper;
